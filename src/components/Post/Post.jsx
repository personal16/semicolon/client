//import { Test } from './Post.styles';

import { Link, Route, BrowserRouter as Router, Switch } from "react-router-dom";
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import ReactHtmlParser from 'react-html-parser';
import configuration from "../../config.json"
import { connect } from 'react-redux';

class Post extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      isLoaded: false,
      item: null,
      id: this.props.match.params.id
    };
  }

 

  componentWillMount = () => {
    console.log('Post will mount');
  }

  componentDidMount = () => {
    var loding=  document.getElementById("loding");
    loding.hidden=false;
    fetch(configuration.API_URL+"Post/GetPostsById/" + this.state.id)
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
          loding.hidden=true;
          this.setState({
            isLoaded: true,
            item: result.data
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
    console.log('Post mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Post will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Post will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('Post did update');
  }

  componentWillUnmount = () => {
    console.log('Post will unmount');
  }

  render() {
    const { error, isLoaded, item } = this.state;

    return (
      <div id="colorlib-main">
        <div className="demo">
          <h1>{item && item.title}</h1>
          <h5>{item && item.userName}</h5>
          <h6>{item && item.persianDate}</h6>
          <hr></hr>
          {item && ReactHtmlParser (item.discretion)}
         
     
        </div>
        <br/>
        {item &&
          item.tags.map(tag => (
            <Link className="nav-link">&nbsp; {"#" + tag.tags} &nbsp;</Link>
          ))}
        <div className="footer">
          This footer will always be positioned at the bottom of the page, but{" "}
          <strong>not fixed</strong>.
        </div>
      </div>
    );
  }
}

Post.propTypes = {
  // bla: PropTypes.string,
};

Post.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Post);
