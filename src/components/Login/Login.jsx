import "../../assets/css/login.css"

import React, { PureComponent } from 'react';
import {SetUser, SetUserAuthentication} from "../../actions/user-action"

import PropTypes from 'prop-types';
import configuration from "../../config.json"
import { connect } from 'react-redux';

//import { Test } from './Login.styles';

class Login extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      username:'',
      password:''
    };
  }

  login(event){
    var loding = document.getElementById("loding");
    loding.hidden = false;
    const requestOptions = {
      method: "POST",
      headers:  { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       },
      body: JSON.stringify({
        userName: this.state.username,
        password: this.state.password
 
      }),
    };
    fetch(configuration.API_URL+"Account/Login", requestOptions)
      .then((response) => response.json())
      .then((data) => {
        loding.hidden = true;
          alert(": )");
          localStorage.setItem('jwt-authenticate', JSON.stringify(data));
   
          this.props.SetUserAuthentication({'authenticate':data});
          this.getUser(data.userName)
        
        console.log(data)
      }) 
      .catch((error) => {
         
    console.log('error: ' + error);
 
  });  

  }

     getUser(username) {
    
     // var loding=  document.getElementById("loding");
    // loding.hidden=false;
    fetch(configuration.API_URL+`Users/getProfile/`+username)
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
          
      this.props.setUser(result.data);
      this.props.history.push(`/@${username}`)
 
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }
  update = (name, e) => {
    this.setState({ [name]: e.target.value });
  };

  componentWillMount = () => {
    console.log('Login will mount');
  }

  componentDidMount = () => {
    console.log('Login mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Login will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Login will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('Login did update');
  }

  componentWillUnmount = () => {
    console.log('Login will unmount');
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div className="LoginWrapper">
 
    <div >
  <div className="imgcontainer">
  <img src="https://www.w3schools.com/howto/img_avatar2.png" alt="Avatar" className="avatar" />
  </div>

  <div className="container">
    <label for="uname"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="uname" required   onChange={(e) => this.update("username", e)}/>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required onChange={(e) => this.update("password", e)} />
        
    <button  onClick={this.login.bind(this)} >Login</button>
    <label>
      <input type="checkbox" checked="checked" name="remember" /> Remember me
    </label>
  </div>

  <div className="container" style={{'backgroundColor':'#f1f1f1'}}>
    <button type="button" className="cancelbtn">Cancel</button>
    <span className="psw">Forgot <a href="#">password?</a></span>
  </div>
  </div>
 
      </div>
    );
  }
}

Login.propTypes = {
  // bla: PropTypes.string,
};

Login.defaultProps = {
  // bla: 'test',
};
const mapStateToProps = state => ({
  // blabla: state.blabla,
    user:state.UserReducer
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
  setUser : (user) => dispatch(SetUser(user)),
  SetUserAuthentication : (info) => dispatch(SetUserAuthentication(info))
});
 
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
