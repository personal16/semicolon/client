import React, { PureComponent } from 'react';

import About from '../About/index'
import Introduction from '../Introduction/index'
import PropTypes from 'prop-types';
import {SetUser} from "../../actions/user-action.js"
import Timeline from '../Timeline/index'
import configuration from "../../config.json"
import { connect } from 'react-redux';

//import { Test } from './Home.styles';

class Home extends PureComponent { 
  constructor(props) {
    super(props);
     
    this.state = {
      hasError: false,
      username : this.props.match.params.username
      
    };
    
 
  }

 
  // componentWillMount = () => {
  //   // var loding=  document.getElementById("loding");
  //   // loding.hidden=false;
  //   fetch(configuration.API_URL+`Users/getProfile/`+this.state.username.replace('@', ''))
  //     .then(res => res.json())
  //     .then(
  //       result => {
  //         console.log(result);
     
  //     this.props.setUser(result.data);
      
 
  //       },
  //       // Note: it's important to handle errors here
  //       // instead of a catch() block so that we don't swallow
  //       // exceptions from actual bugs in components.
  //       error => {
  //         this.setState({
  //           isLoaded: true,
  //           error
  //         });
  //       }
  //     );
  // }

  componentDidMount = () => {
    console.log('Home mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Home will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Home will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('Home did update');
  }

  componentWillUnmount = () => {
    console.log('Home will unmount');
  }
  render() {
    return (
   	 <div id="colorlib-main">

					<Introduction></Introduction>
					<About></About>
					<Timeline></Timeline>
          	</div> 
    )
  }
}

Home.propTypes = {
  // bla: PropTypes.string,
};

Home.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = (state) => {
  return {
   
    users: state.UserReducer
  };
};

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
  setUser : (user) => dispatch(SetUser(user))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
