//import { Test } from './Cv.styles';
import "../../assets/css/cv.css";

import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class Cv extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentWillMount = () => {
    console.log('Cv will mount');
  }

  componentDidMount = () => {
    console.log('Cv mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Cv will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Cv will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('Cv did update');
  }

  componentWillUnmount = () => {
    console.log('Cv will unmount');
  }

  render() {
    return (
      <section id="lab_resume">
        <div className="container">
          <div className="row">
            <div className="resume">
              <div className="r-sidebar">
                <div className="r-sidebar-item">
                  <div className="img">
                    <a href="#">
                      <img src="images/Avatar.jpg" alt="cheryl"></img>
                    </a>
                  </div>

                  <div className="name">
                    <h3>Sobhan Mozafari</h3>
                    <small>I Design Business Websites</small>
                  </div>
                  <div className="clearfix"></div>
                </div>

                <div className="r-detail">
                  <table>
                    <tbody>
                      <tr>
                        <th>Name</th>
                        <td>Sobhan Mozafari</td>
                      </tr>
                      <tr>
                        <th>D.O.B</th>
                        <td>November 2st 1995</td>
                      </tr>
                      <tr>
                        <th>Age</th>
                        <td>25 Yrs</td>
                      </tr>
                      <tr>
                        <th>Qualification</th>
                        <td>M.E., P.hD</td>
                      </tr>
                      <tr>
                        <th>Mobile</th>
                        <td>9185688292</td>
                      </tr>
                      <tr>
                        <th>Email</th>
                        <td>sobhan.plus.plus@gmail.com</td>
                      </tr>
                      <tr>
                        <th>Location</th>
                        <td>Tehran</td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                <div className="social">
                  <a
                    target="_blank"
                    href="https://www.facebook.com/Sobi1995"
                    className="facebook"
                  >
                    <i className="fa fa-facebook"></i>
                  </a>
                  <a
                    target="_blank"
                    href="https://twitter.com/Sobhan__1995"
                    className="twitter"
                  >
                    <i className="fa fa-twitter"></i>
                  </a>
                  <a
                    target="_blank"
                    href="https://www.linkedin.com/in/sobi-mozafari-261993133/"
                    className="linkedin"
                  >
                    <i className="fa fa-linkedin"></i>
                  </a>
                  <a
                    target="_blank"
                    href="https://www.instagram.com/sobi.m1995/"
                    className="instagram"
                  >
                    <i className="fa fa-instagram"></i>
                  </a>
                  <a
                    target="_blank"
                    href="https://github.com/SobhanPlusPlus"
                    className="github"
                  >
                    <i className="fa fa-github"></i>
                  </a>
                  <a
                    target="_blank"
                    href="https://t.me/sobi_1995"
                    className="telegram"
                  >
                    <i className="fa fa-telegram"></i>
                  </a>
                </div>
              </div>

              <div className="main-block">
                <div className="objective">
                  <h3>Welcome</h3>
                  <div className="objective-content">
                    <p>
                      Hi!, I'm Cheryl from Boston, MA! Nemo enim ipsam
                      voluptatem quia voluptas sit aspernatur aut odit aut
                      fugit, sed quia consequuntur magni dolores eos qui ratione
                      voluptatem sequi nesciunt nui dolorem ipsum quia dolor sit
                      amet consectetur.
                    </p>
                  </div>
                </div>
                <hr />

                <div className="skill">
                  <h3>Skills</h3>

                  <div className="skill-content">
                    <span className="big-circle">
                      Web Api<span className="small-circle bg-white">70</span>
                    </span>
                    <span className="big-circle">
                      .Net Core<span className="small-circle bg-white">60</span>
                    </span>
                    <span className="big-circle">
                      Socket <span className="small-circle bg-white">60</span>
                    </span>
                    <span className="big-circle">
                      Angular<span className="small-circle bg-white">70</span>
                    </span>
                    <span className="big-circle">
                      React-js<span className="small-circle bg-white">50</span>
                    </span>
                    <span className="big-circle">
                      Git<span className="small-circle bg-white">50</span>
                    </span>
                    <span className="big-circle">
                      jQuery<span className="small-circle bg-white">70</span>
                    </span>
                    <span className="big-circle">
                      SqlServer<span className="small-circle bg-white">40</span>
                    </span>
                  </div>
                </div>
                <hr />

                <div className="education">
                  <h3>Education</h3>
                  <div className="row">
                    <div className="col-md-4 col-sm-4">
                      <div className="education-item">
                        <h6>[ 2002 - 2006 ]</h6>
                        <h4>Bachelor of Something Something</h4>
                        <p>
                          Lorem ipsum dolor sit ametconsectetur adipisicing
                          elit, sed do eiusmod tempor incididunt ut labore et
                          dolore.
                        </p>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-4">
                      <div className="education-item">
                        <h6>[ 2006 - 2008 ]</h6>
                        <h4>Master of Something Important</h4>
                        <p>
                          Lorem ipsum dolor sit ametconsectetur adipisicing
                          elit, sed do eiusmod tempor incididunt ut labore et
                          dolore.
                        </p>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-4">
                      <div className="education-item">
                        <h6>[ 2008 - 2010 ]</h6>
                        <h4>Doctorate in Really Important Stuff</h4>
                        <p>
                          Lorem ipsum dolor sit ametconsectetur adipisicing
                          elit, sed do eiusmod tempor incididunt ut labore et
                          dolore.
                        </p>
                      </div>
                    </div>
                    <div className="clearfix"></div>
                  </div>
                </div>
                <hr />

                <div className="experience">
                  <h3>Experience</h3>
                  <div className="row">
                    <div className="col-md-4 col-sm-4">
                      <div className="experience-item">
                        <div className="experience-detail">
                          <h6>[ Sep 2013 To Present ]</h6>
                          <h4>Sr. Web Designer at Somewhere</h4>
                        </div>
                        <p>
                          {" "}
                          Nemo enim ipsam voluptatem quia voluptas sit
                          aspernatur aut odit aut fugit, sed quia consequuntur
                          magni dolores.
                        </p>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-4">
                      <div className="experience-item">
                        <div className="experience-detail">
                          <h6>[ Aug 2012 To Sep 2013 ]</h6>
                          <h4>Web Designer at Another Place</h4>
                        </div>
                        <p>
                          {" "}
                          Nemo enim ipsam voluptatem quia voluptas sit
                          aspernatur aut odit aut fugit, sed quia consequuntur
                          magni dolores.
                        </p>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-4">
                      <div className="experience-item">
                        <div className="experience-detail">
                          <h6>[ Oct 2011 To Aug 2012 ]</h6>
                          <h4>Web Developer at You Know Where</h4>
                        </div>
                        <p>
                          {" "}
                          Nemo enim ipsam voluptatem quia voluptas sit
                          aspernatur aut odit aut fugit, sed quia consequuntur
                          magni dolores.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <hr />

                <div className="copy white"></div>
              </div>

              <div className="clearfix"></div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

Cv.propTypes = {
  // bla: PropTypes.string,
};

Cv.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Cv);
