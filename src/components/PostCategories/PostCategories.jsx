import "../../assets/css/Post-of-Categories.css";

import { Link, Route, BrowserRouter as Router, Switch } from "react-router-dom";
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import configuration from "../../config.json"
import { connect } from 'react-redux';

//import { Test } from './PostCategories.styles';

class PostCategories extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      id: this.props.match.params.id,
      tags: []
    };
  }
  getTags(dataoftags) {
    var tagsinarry = dataoftags.data.map(function(town) {
      return town.tags;
    });
    let tags = [];
    tagsinarry.forEach(element => {
      tags = tags.concat(element);
    });
    return tags;
  }

 
  componentWillMount = () => {
    console.log('PostCategories will mount');
  }

  componentDidMount = () => {
    console.log('PostCategories mounted');
    var loding=  document.getElementById("loding");
    loding.hidden=false;
    fetch(
      configuration.API_URL+"Post/GetPostsByCategoriesId/" +
        this.state.id
    )
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
           ;
          loding.hidden=true;
          this.setState({
            isLoaded: true,
            items: result.data,
            tags: this.getTags(result)
          });
        },

        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('PostCategories will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('PostCategories will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('PostCategories did update');
  }

  componentWillUnmount = () => {
    console.log('PostCategories will unmount');
  }

  render() {
    const { error, isLoaded, items, tags } = this.state;
    const{userName} =this.props.user.user;
    return (
      <div id="colorlib-main">
        <div className="content container">
          <h1 className="title" dir="rtl">
            مقالات
          </h1>
          <hr />

          <ul className="posts" dir="rtl">
            {items &&
              items.map(item => (
                <li  key={item.id}>
                  <span>
                    <Link to={`/@${userName}/Post/` + item.id} className="nav-link">
                      {item.title}
                    </Link>
                    &nbsp; » &nbsp;
                    <time className="post-list" style={{ "fontSize": "12px" }}>
                      {item.persianDate}
                    </time>
                    <br />
                    <div></div>
                  </span>
                </li>
              ))}
            <br />

            {tags &&
              tags.map((tag,i) => (
                <Link  key={i} to={`/@${userName}/tags-of-post/` + tag.tags} className="nav-link">&nbsp; {"#" + tag.tags} &nbsp;</Link>
              ))}
          </ul>
        </div>
      </div>
    );
  }

}

PostCategories.propTypes = {
  // bla: PropTypes.string,
};

PostCategories.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
  user:state.UserReducer
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PostCategories);
