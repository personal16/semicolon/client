import "../../assets/css/contact-me.css";

import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import configuration from "../../config.json"
import { connect } from 'react-redux';

//import { Test } from './ContactMe.styles';

class ContactMe extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      data: [
        {
          name: "",
          email: "",
          subject: "",
          message: "",
        },
      ],
    };
  }
  SendMessage(event) {
    var loding = document.getElementById("loding");
    loding.hidden = false;
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        name: this.state.name,
        email: this.state.email,
        subject: this.state.subject,
        message: this.state.message,
      }),
    };
    fetch(configuration.API_URL+"ContactMe", requestOptions)
      .then((response) => response.json())
      .then((data) => {
        loding.hidden = true;
        // alert(": )");
      });
  }
  update = (name, e) => {
    this.setState({ [name]: e.target.value });
  };
  componentWillMount = () => {
    console.log('ContactMe will mount');
  }

  componentDidMount = () => {
    console.log('ContactMe mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('ContactMe will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('ContactMe will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('ContactMe did update');
  }

  componentWillUnmount = () => {
    console.log('ContactMe will unmount');
  }

  render() {
    return (
      <div id="colorlib-main">
        <div className="bodydiv">
          <section>
            <div>
              <div className="row">
                <div className="col-sm-12">
                  <div className="row">
                    <div className="col-sm-8 col-sm-offset-2">
                      <input type="hidden" data-form-email="true" />
                      <div className="form-group">
                        <input
                          type="text"
                          className="form-control"
                          required=""
                          placeholder="Name*"
                          data-form-field="Name"
                          onChange={(e) => this.update("name", e)}
                          name="name"
                        />
                      </div>
                      <div className="form-group">
                        <input
                          type="email"
                          className="form-control"
                          required=""
                          placeholder="Email*"
                          data-form-field="Email"
                          onChange={(e) => this.update("email", e)}
                          email=""
                        />
                      </div>
                      <div className="form-group">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="subject"
                          data-form-field="subject"
                          onChange={(e) => this.update("subject", e)}
                          name="subject"
                        />
                      </div>
                      <div className="form-group">
                        <textarea
                          className="form-control"
                          placeholder="message"
                          rows="7"
                          data-form-field="message"
                          onChange={(e) => this.update("message", e)}
                          name="message"
                        ></textarea>
                      </div>
                      <div>
                        <button
                          type="button"
                          onClick={this.SendMessage.bind(this)}
                          className="btn btn-lg btn-danger"
                        >
                          CONTACT US
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}

ContactMe.propTypes = {
  // bla: PropTypes.string,
};

ContactMe.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ContactMe);
