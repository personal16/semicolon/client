import { Link, Route, BrowserRouter as Router, Switch } from "react-router-dom";
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import configuration from "../../config.json"
import { connect } from 'react-redux';

//import { Test } from './Timeline.styles';

class Timeline extends PureComponent { 
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }
 
  componentWillMount = () => {
    console.log('Timeline will mount');
  }

  componentDidMount = () => {
    console.log('Timeline mounted');

  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Timeline will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Timeline will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('Timeline did update');
  }

  componentWillUnmount = () => {
    console.log('Timeline will unmount');
  }

  render() {
  
    const{userName} =this.props.user.user;
    const {newPosts} =this.props.user;
    return (
      <div>
        <section className="colorlib-experience" data-section="timeline">
          <div className="colorlib-narrow-content">
            <div className="row">
              <div
                className="col-md-6 col-md-offset-3 col-md-pull-3 animate-box"
                data-animate-effect="fadeInLeft"
              >
                <span className="heading-meta">News</span>
                <h2 className="colorlib-heading animate-box">پست های جدید</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="timeline-centered">
                  {newPosts &&
                    newPosts.map(item => (
                      <article
                        key={item.id}
                        className="timeline-entry animate-box"
                        data-animate-effect="fadeInLeft"
                      >
                        <div className="timeline-entry-inner">
                          <div className="timeline-icon color-3">
                            <i className="icon-pen2" />
                          </div>
                          <div className="timeline-label">
                            <h2>
                              <Link to={`/@${userName}/Post/`+item.id} className="nav-link"  >
                                {item.title}
                              </Link>
                              <span style={{'float':'left'}}>{item.persianDate} </span>
                            </h2>
                            <p>{item.discretion}</p>
                          </div>
                        </div>
                      </article>
                    ))}
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

Timeline.propTypes = {
  // bla: PropTypes.string,
};

Timeline.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
  user:state.UserReducer
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Timeline);
