import { Link, Route, BrowserRouter as Router, Switch } from "react-router-dom";
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

//import { Test } from './Sidebar.styles';

class Sidebar extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentWillMount = () => {
    console.log('Sidebar will mount');
  }

  componentDidMount = () => {
    console.log('Sidebar mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Sidebar will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Sidebar will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('Sidebar did update');
  }

  componentWillUnmount = () => {
    console.log('Sidebar will unmount');
  }

  render() {
       
    const { user } = this.props.user;
    const { userName } = this.props.user;
 
    
    return (
      <div>
        <div>
          <nav
            href="#navbar"
            className="js-colorlib-nav-toggle colorlib-nav-toggle"
            data-toggle="collapse"
            data-target="#navbar"
            aria-expanded="false"
            aria-controls="navbar"
          >
            <i />
          </nav>
          <aside id="colorlib-aside" className="border js-fullheight">
            <div className="text-center">
              <div
                className="author-img"
                style={{ backgroundImage: `url(${user.photo})` }}
              />
              <h1 id="colorlib-logo">
              <a >{user && user.fName +' '+ user.lName} </a>
              <a >{user && user.username} </a>
              </h1>
              <span className="email">
                <i className="icon-mail"></i> user.email
              </span>
            </div>



            <nav id="colorlib-main-menu">
              <ul>
              { user.soshalNetwork &&
                 user.soshalNetwork.map((soshal, i) =>  
                 
                  <li key={i} >
                  <a
                    href={soshal.url}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <i  className={`icon-${soshal.name}`} />
                  </a>
                </li>
            
                  )            
              }
            
         
             
       
              
              </ul>
            </nav>
            <div className="colorlib-footer">

                <ul>
                  <li>
                    <span>
                      <Link to={`/@${user.userName}`} className="nav-link-sliser">
                        خانه
                      </Link>
                    </span>
                  </li>
                  <li>
                  <span>
                    <Link  to={`/@${user.userName}/contact-me`} className="nav-link-sliser">
                      ارتباط با من
                    </Link>
                  </span>
                </li>
                <li>
                <span>
                  <Link target="_blank" to={`/@${user.userName}/cv`} className="nav-link-sliser">
                     رزومه من
                  </Link>
                </span>
              </li>

              <li>
              <span>
                <Link   to={`/@semicolon/login/`} className="nav-link-sliser">
                 ورود
                </Link>
              </span>
            </li> 

{ 
   
  user.userName &&
  user.userName ==="semicolon"  ?(
            <li>
            <span>
              <Link to={`/@semicolon/user/`} className="nav-link-sliser">
               کاربران
              </Link>
            </span>
          </li>) : null

}
                </ul>

            </div>
          </aside>
        </div>
      </div>
    );
  }
}

Sidebar.propTypes = {
  // bla: PropTypes.string,
};

Sidebar.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
    user:state.UserReducer
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Sidebar);
