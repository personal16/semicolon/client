import "../../assets/css/style-me.css";

import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
//import { Test } from './Me.styles';
import configuration from "../../config.json"
import { connect } from 'react-redux';

class Me extends PureComponent { 
  constructor(props) {
    super(props);
 
    var pathname = new URL(window.location).pathname.split("/");
 
    
    this.state = {
    
      username : pathname[1],
      profile:''
      
    };
  }

  componentWillMount = () => {
    // var loding=  document.getElementById("loding");
    // loding.hidden=false;
    fetch(configuration.API_URL+`Users/getProfile/`+this.state.username.replace('@', ''))
      .then(res => res.json())
      .then(
        result => {
         
     this.setState({profile:result.data});
     console.log(this.state.profile);
      
 
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  componentDidMount = () => {
    console.log('Me mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Me will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Me will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('Me did update');
  }

  componentWillUnmount = () => {
    console.log('Me will unmount');
  }

  render () {
     
    const {user} =this.state.profile
    return (
      <div className="MeWrapper">
      <div className="user-profile__wrapper">
      <div className="user-profile__box">
        <div className="user-profile__img">
          <img src={ user && user.photo} />
        </div>
        <div className="user-profile__txt">
          <h3>{user && user.lName} {user && user.lFame}</h3>
          <h4>{ user && user.userName}</h4>
          <p className="about-me">{user && user.aboutMe}</p>
        </div>
        <div className="user-profile__social">
        { user &&
          user.soshalNetwork.map((soshal, i) =>  
          <a key={i} href="" className={`user-${soshal.name}`}><i className={`fa fa-${soshal.name}`} aria-hidden="true"></i></a>
          )            
        }
      
        </div>
      </div>
    </div>
      </div>
    );
  }
}

Me.propTypes = {
  // bla: PropTypes.string,
};

Me.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Me);
