import "../../assets/css/Slider.css";

import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import configuration from "../../config.json"
import { connect } from 'react-redux';

//import { Test } from './Introduction.styles';

class Introduction extends PureComponent { 
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      item: { me: "", programming: "" },
      slideIndex: 0,
    };
 
  }
  
  showSlides(n) {
      console.log(n)
          
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }

    if (n > slides.length) {
      n = 1;
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }

    slides[n - 1].style.display = "block";
    dots[n - 1].className += " active";
   // setTimeout(this.showSlides(n+1), 5000); // Change image every 2 seconds
  }
  componentWillMount = () => {
    console.log('Introduction will mount');
  }

  componentDidMount = () => {
    
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Introduction will receive props', nextProps);
    this.showSlides(1)
  }

  componentWillUpdate = (nextProps, nextState) => {
   
    console.log('Introduction will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
 
    console.log('Introduction did update');
  }

  componentWillUnmount = () => {
    console.log('Introduction will unmount');
  }

  render() {
    const { error, isLoaded, item } = this.state;
     
    const { sliderImage } = this.props.user.user;
    return (
      <div>
    
        <section
          id="colorlib-hero"
          className="js-fullheight"
          data-section="home"
        >
          <div className="slideshow-container">
            <div className="mySlides" >
              <div className="numbertext">1 / 3</div>
              <img
                src={sliderImage &&  sliderImage[0]}
                className="sliderimage"
              />
              <div className="text">Caption Text</div>
            </div>

            <div className="mySlides">
              <div className="numbertext">2 / 3</div>
              <img
              src={sliderImage &&   sliderImage[1]}
                className="sliderimage"
              />
              <div className="text">Caption Two</div>
            </div>

            <div className="mySlides">
              <div className="numbertext">3 / 3</div>
              <img
                src={ sliderImage &&  sliderImage[2]}
                className="sliderimage"
              />
              <div className="text">Caption Three</div>
            </div>
          </div>
          <br />

          <div style={{ textAlign: "center" }}>
            <span onClick={() => this.showSlides(1)} className="dot"></span>
            <span onClick={() => this.showSlides(2)} className="dot"></span>
            <span onClick={() => this.showSlides(3)} className="dot"></span>
          </div>
        </section>
      </div>
    );
  }
}

Introduction.propTypes = {
  // bla: PropTypes.string,
};

Introduction.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
      user:state.UserReducer
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Introduction);
