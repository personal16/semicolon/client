import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

//import { Test } from './Users.styles';

class Users extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentWillMount = () => {
    console.log('Users will mount');
  }

  componentDidMount = () => {
    console.log('Users mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Users will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Users will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('Users did update');
  }

  componentWillUnmount = () => {
    console.log('Users will unmount');
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div className="UsersWrapper">
      <h1>  Test content</h1> 
      </div>
    );
  }
}

Users.propTypes = {
  // bla: PropTypes.string,
};

Users.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Users);
