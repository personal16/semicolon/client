//import { Test } from './About.styles';

import { Link, Route, BrowserRouter as Router, Switch } from "react-router-dom";
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import configuration from "../../config.json"
import { connect } from 'react-redux';

class About extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      isLoaded: false,
      items: []

    };
  }

  componentWillMount = () => {
    console.log('About will mount');
  }

  // componentDidMount = () => {
  //   var loding=  document.getElementById("loding");
  //   loding.hidden=false;
  //   fetch(configuration.API_URL+"/Categories/GetCategories")
  //     .then(res => res.json())
  //     .then(
  //       result => {
  //         console.log(result);
  //         loding.hidden=true;
  //         this.setState({
  //           isLoaded: true,
  //           items: result.data
  //         });
  //       },
  //       // Note: it's important to handle errors here
  //       // instead of a catch() block so that we don't swallow
  //       // exceptions from actual bugs in components.
  //       error => {
  //         this.setState({
  //           isLoaded: true,
  //           error
  //         });
  //       }
  //     );
  // }

  componentWillReceiveProps = (nextProps) => {
    console.log('About will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('About will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('About did update');
  }

  componentWillUnmount = () => {
    console.log('About will unmount');
  }

  render () {
    // const { error, isLoaded, items } = this.state;
    const { categories } = this.props.user;
     
    const{userName} =this.props.user.user;
  
     const{aboutMe} =this.props.user.user;
    return (
      <div>
      <section className="colorlib-about" data-section="about">
      <div className="colorlib-narrow-content">
          <div className="row">
          <div className="col-md-12">
              <div className="row row-bottom-padded-sm animate-box" data-animate-effect="fadeInLeft">
              <div className="col-md-12">
                  <div className="about-desc">
                  <span className="heading-meta">About Us</span>
                  <h2 className="colorlib-heading">Who Am I?</h2>
                  <Link target="_blank" to={`/@${userName}/me`} className="nav-link-sliser">
                  {aboutMe &&  aboutMe.slice(0, 500)}
                  </Link>
                 
                  </div>
              </div>
              </div>
          </div>
          </div>
      </div>
      </section>
      <section className="colorlib-about">
      <div className="colorlib-narrow-content">
          <div className="row">
          <div className="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
              <span className="heading-meta">Category </span>
              <h2 className="colorlib-heading">دسته بندی</h2>
          </div>
          </div>
          <div className="row row-pt-md">
          {categories && 
            categories.map(item => (
                <div key={item.id} className="col-md-4 text-center animate-box">
                    <div className="services color-1">
                    <span className="icon">
                        <i className="icon-bulb" />
                    </span>
                    <div className="desc">
                   
                        <Link to={`/@${userName}/Post-of-Categories/`+item.id} className="nav-link"  >
                        <h3>{item.name} </h3>
                      </Link>
                        <p>{item.discretion}</p>
                    </div>
                    </div>
                </div>
                ))}
          </div>
      </div>
      </section>
    </div>
    );
  }
}

About.propTypes = {
  // bla: PropTypes.string,
};

About.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
  user:state.UserReducer
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(About);
