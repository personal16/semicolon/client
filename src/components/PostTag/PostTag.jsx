//import { Test } from './PostTag.styles';

import "../../assets/css/post-tag.css";

import React, { PureComponent } from 'react';

import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import configuration from "../../config.json"
import { connect } from 'react-redux';

class PostTag extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      isLoaded: false,
      items: [],
       
    };
  }
   

  componentWillMount = () => {
    console.log('PostTag will mount');
  }

  componentDidMount = () => {
    console.log('PostTag mounted');
    this.getPost(this.props.match.params.tag);
  }
  getPost(tag){
    var loding=  document.getElementById("loding");
    loding.hidden=false;
    
    fetch(
      configuration.API_URL+ "Post/GetPostsBytag/" +tag
    )
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
         
          loding.hidden=true;
          this.setState({
            isLoaded: true,
            items: result.data,

          });
        },

        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('PostTag will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('PostTag will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('PostTag did update');
  }

  componentWillUnmount = () => {
    console.log('PostTag will unmount');
  }

  render() {
    const { error, isLoaded, items, tags } = this.state;
    return (
      <div id="colorlib-main">
        <div className="content container">
          <h1 className="title" dir="rtl">
            مقالات
          </h1>
          <hr />

          <ul className="posts" dir="rtl">
            {items &&
              items.map((item) => (
                <li    key={item.id} >
                  <span>
                    <Link to={"/Post/" + item.id} className="nav-link">
                      {item.title}
                    </Link>
                    &nbsp; » &nbsp;
                    <time className="post-list" style={{ 'fontSize': '12px' }}>
                      {item.persianDate}
                    </time>
                    <br />
                    <div id="tag" style={{ 'fontSize': 'x-small' }}>
                      {item.tags &&
                        item.tags.map((tag,i) => (
                          <Link
                          onClick={() => this.getPost(tag.tags)}
                          key={i}
                            className="nav-link"
                          >
                            &nbsp; {"#" + tag.tags} &nbsp;

                          </Link>
                        ))}
                    </div>
                    <div></div>
                  </span>
               
                </li>
              
              ))}
            <br />
          </ul>
        </div>
      </div>
    );
  }
}

PostTag.propTypes = {
  // bla: PropTypes.string,
};

PostTag.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PostTag);
