
export const SET_USER = "SET_USER";
export const GET_USER = "GET_USER";

export const SET_USER_Authentication = "SET_USER_Authentication";
export const GET_USER_Authentication = "GET_USER_Authentication";

export function SetUser(user) {
  return {
    type: SET_USER,
    payload: user,
  };
}

export function GetUser() {
  return {
    type: GET_USER
  
  };
}

export function SetUserAuthentication(info) {
  return {
    type: SET_USER_Authentication,
    payload: info,
  };
}

export function GetUserAuthentication() {
  return {
    type: GET_USER_Authentication
  
  };
}

