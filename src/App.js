import "./App.css";

import { Link, Route, BrowserRouter as Router, Switch } from "react-router-dom";
import React, { Component } from "react";

import { BrowserRouter } from "react-router-dom";
import ContactMe from "./components/ContactMe";
import Cv from "./components/Cv/index";
import Home from "./components/Home/index";
import LodingHttp from "./Core/LodingHttp";
import Login from "./components/Login"
import Main from "./Core/layout/Main"
import Me from  "./components/Me"
import Nothing from "./Core/layout/Nothing"
import Post from "./components/Post/index";
import PostCategories from "./components/PostCategories/index";
import PostTag from "./components/PostTag/index";
import {SetUser} from "./actions/user-action"
import configuration from "./config.json"
import { connect } from 'react-redux';
import user from "./components/Users/index"

class App extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
       
      var pathname = new URL(window.location).pathname.split("/")[1]
    pathname= pathname=="" ? "semicolon":pathname; 
    
    this.state = {
    
      username : pathname
      
    };
 
  }
 componentWillMount = () => {
    // var loding=  document.getElementById("loding");
    // loding.hidden=false;
    fetch(configuration.API_URL+`Users/getProfile/`+this.state.username.replace('@', ''))
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
     
           
      this.props.setUser(result.data);
      
 
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }
  render() {
    const routes = [
      {
        layout:Main,
        subRoutes:[
          {
            path:"/:username/Post-of-Categories/:id",
            component:PostCategories
          },
          {
            path:"/:username/tags-of-post/:tag",
            component:PostTag
          },
          {
            path:"/:username/Post/:id",
            component:Post
          },
          {
            path:"/:username/user/",
            component:user
          },
          {
            path:"/:username/contact-me",
            component:ContactMe
          },
          {
            exact:true,
            path:"/",
            component:Home
          },
          {
            exact:true,
            path:"/:username/",
            component:Home
          }
        ]
      },
      {
        layout:Nothing,
        subRoutes:[
          {
            path:"/:username/login/",
            component:Login
          },
          {
            path:"/:username/cv/",
            component:Cv
          },
        
          {
            path:"/:username/me/",
            component:Me
          }
          
        ]
      }
    ];
    return (
      <div id="colorlib-page">
        <LodingHttp />
        <Router>
        <Switch>
        {routes.map((route,i)=>
          <Route key={i} exact={route.subRoutes.some(r=>r.exact)} path={route.subRoutes.map(r=>r.path)}>
            <route.layout>
              {route.subRoutes.map((subRoute,i)=>
                <Route key={i} {...subRoute} />
              )}
            </route.layout>
          </Route>
        )}
      </Switch>
     </Router>
      </div>
    );
  }
}
App.propTypes = {
  // bla: PropTypes.string,
};

App.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
    user:state.UserReducer
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
  setUser : (user) => dispatch(SetUser(user))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);

 
