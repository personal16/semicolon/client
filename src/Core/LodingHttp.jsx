import "../assets/css/loding.css"

import React, { Component } from "react";

export default class LodingHttp extends Component {
  render() {
    return (
      <div hidden  id="loding" style={{
 
      
        'width': '100%',
        'position': 'fixed',
        'height': '100%',
    
    }}>
    
    <div className="glitch">

  <span>;</span>
</div>
      </div>
    );
  }
}
