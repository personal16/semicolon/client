import PropTypes from 'prop-types';
import React from 'react';
import Sidebar from "../../components/Sidebar/index";

//import { Test } from './Nothing.styles';

const Main = (props) => (
  <div className="NothingWrapper">
 
  <div id="container-wrap">
  <div className="sideBar">
 
   <Sidebar/> 
 

  </div>
    <div className="compBox">
    
    {props.children}
    </div>
  </div>
 
  </div>
);

Main.propTypes = {
  // bla: PropTypes.string,
};

Main.defaultProps = {
  // bla: 'test',
};

export default Main;
