import PropTypes from 'prop-types';
import React from 'react';
//import { Test } from './Nothing.styles';

const Nothing = (props) => (
  <div className="NothingWrapper">
  {props.children}
  </div>
);

Nothing.propTypes = {
  // bla: PropTypes.string,
};

Nothing.defaultProps = {
  // bla: 'test',
};

export default Nothing;
